from django.conf.urls import patterns, url

from contacts import views

urlpatterns = patterns('',
    # ex: /contacts/
    url(r'^$', views.index, name='contacts_all'),
    # ex: /contacts/5/
    url(r'^(?P<contact_id>\d+)/$', views.detail, name='contacts'),
    )
