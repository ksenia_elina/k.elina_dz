from django.shortcuts import render
from .models import Contacts
from django.http import HttpResponse

def index(request):
    latest_contacts_list = Contacts.objects.all()
    context = {'contacts_list': latest_contacts_list }
    return render(request, 'contacts/contacts_base.html', context)
    

def detail(request, contact_id):
    return HttpResponse("You're looking at question %s." % contact_id)
