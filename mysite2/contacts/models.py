from django.db import models

class Contacts(models.Model):
    contact_name = models.CharField(max_length=200)
    contact_ph = models.CharField(max_length=200)
