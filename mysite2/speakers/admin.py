from django.contrib import admin
from speakers.models import Name

class AdminAdmin(admin.ModelAdmin):
    list_display = ('Name_fio', 'pub_date')
    search_fields = ['Name_fio']
    
admin.site.register( Name, AdminAdmin)
