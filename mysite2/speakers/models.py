from django.db import models
from django.core.exceptions import ValidationError
import datetime

class Name(models.Model):
    Name_fio = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published', default = datetime.datetime.now() )

    def __unicode__(self):
        return self.Name_fio
        
    @models.permalink    
    def get_absolute_url(self):
        return('speakers', [str(self.id)])
    
    def clean(self):
        num_words = len(self.Name_fio)
        if num_words < 3:
            raise ValidationError('< 3 simbols!')
        else:
            return self.Name_fio