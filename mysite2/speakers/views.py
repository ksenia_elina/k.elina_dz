from .models import Name
from .forms import Speakers
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.cache import cache_page

@cache_page(60*15)
def listing(request):
    speakers_list = Name.objects.all()
    paginator = Paginator(speakers_list, 2) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        name = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        name = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        name = paginator.page(paginator.num_pages)
        
    speakerform = Speakers()
    if request.method == 'POST':
         speakerform = Speakers(request.POST)
         if speakerform.is_valid():
            speakerform.save()
    context = {'speakers_list': name, 'form':speakerform  }
    return render(request, 'speakers/speakers_base.html', context)

def index(request):
    #latest_name_list = Name.objects.all()
    latest_name_list = Name.objects.filter(pub_date__year = 2015)
    speakerform = Speakers()
    if request.method == 'POST':
         speakerform = Speakers(request.POST)
         if speakerform.is_valid():
            speakerform.save()
    context = {'speakers_list': latest_name_list, 'form':speakerform  }
    return render(request, 'speakers/speakers_base.html', context)

def detail(request, speakers_id):
    s = Name.objects.get(id = speakers_id)
    form = Speakers(instance = s)
    if request.method == 'POST':
         form = Speakers(request.POST, instance = s)
         if form.is_valid():
            print 'valid'
            #s.Name_fio = form.cleaned_data['Name_fio']
            form.save()
         else:
             print 'invalid'
    context = {'form':form  }
    return render(request, 'speakers/speakers_detail.html', context)
    
def delete_new(request, speakers_id):
    new_to_delete = get_object_or_404(Name, id=speakers_id)
    form = Speakers(instance=new_to_delete)
    #+some code to check if this object belongs to the logged in user

    if request.method == 'POST':
        form = Speakers(request.POST, instance=new_to_delete)

        if form.is_valid(): # checks CSRF
            new_to_delete.delete()
            return redirect("/speakers/") # wherever to go after deleting

    else:
        form =Speakers(instance=new_to_delete)

    template_vars = {'form': form}
    return render(request, 'speakers/delete_new.html', template_vars)