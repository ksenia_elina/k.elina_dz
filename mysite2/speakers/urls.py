from django.conf.urls import patterns, url

from speakers import views

urlpatterns = patterns('',
    # ex: /contacts/
    url(r'^$', views.listing, name='speakers_all'),
    # ex: /contacts/5/
    url(r'^(?P<speakers_id>\d+)/$', views.detail, name='speakers'),
    url(r'^(?P<speakers_id>\d+)/delete_new/$', views.delete_new, name='delete_new'),
    )