# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('speakers', '0002_auto_20150621_1512'),
    ]

    operations = [
        migrations.AlterField(
            model_name='name',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 21, 15, 28, 36, 513000), verbose_name=b'date published'),
        ),
    ]
