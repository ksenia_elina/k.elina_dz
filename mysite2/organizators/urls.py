from django.conf.urls import patterns, url
from organizators import views


urlpatterns = patterns('',
    # ex: /contacts/
    url(r'^$', views.index, name='org_all'),
    # ex: /contacts/5/
    url(r'^(?P<organizators_id>\d+)/$', views.detail, name='contacts'),
	)