from django.shortcuts import render
from .models import Org
from django.http import HttpResponse

def index(request):
    latest_org_list = Org.objects.all()
    context = {'org_list': latest_org_list }
    return render(request, 'organizators/organizators_base.html', context)

def detail(request, organizators_id):
    return HttpResponse("You're looking at question %s." % organizators_id)
