from django.db import models

class News(models.Model):
    news_name = models.CharField(max_length=200)
    text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')