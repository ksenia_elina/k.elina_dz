from django.shortcuts import render
from .models import News
from django.http import HttpResponse

def index(request):
    latest_news_list = News.objects.order_by('-pub_date')[:5]
    context = {'news_list': latest_news_list }
    return render(request, 'news/news_base.html', context)

def detail(request, news_id):
    return HttpResponse("You're looking at question %s." % news_id)
