from django.conf.urls import patterns, url

from news import views

urlpatterns = patterns('',
    # ex: /news/
    url(r'^$', views.index, name='news_all'),
    # ex: /news/5/
    url(r'^(?P<news_id>\d+)/$', views.detail, name='news'),
	)