from django.shortcuts import render

from django.http import HttpResponse

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    output = ', '.join([p.question_text for p in latest_question_list])
    return HttpResponse(output)

def detail(request, visryplenia_id):
    return HttpResponse("You're looking at question %s." % visryplenia_id)
