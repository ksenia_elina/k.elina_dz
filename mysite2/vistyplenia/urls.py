from django.conf.urls import patterns, url

from vistyplenia import views

urlpatterns = patterns('',
    # ex: /contacts/
    url(r'^$', views.index, name='vist_all'),
    # ex: /contacts/5/
    url(r'^(?P<vistyplenia_id>\d+)/$', views.detail, name='vistyplenia'),
	)