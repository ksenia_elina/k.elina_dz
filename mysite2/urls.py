from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^contacts/', include('contacts.urls')),
	url(r'^news/', include('news.urls')),
	url(r'^organizators/', include('organizators.urls')),
	url(r'^partners/', include('partners.urls')),
	url(r'^vistyplenia/', include('vistyplenia.urls')),
    url(r'^admin/', include(admin.site.urls)),
)