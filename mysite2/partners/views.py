from django.shortcuts import render
from .models import Partners
from django.http import HttpResponse

def index(request):
    latest_partners_list = Partners.objects.all()
    context = {'partners_list': latest_partners_list }
    return render(request, 'partners/partners_base.html', context)

def detail(request, partners_id):
    return HttpResponse("You're looking at question %s." % partners_id)