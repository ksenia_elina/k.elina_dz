from django.conf.urls import patterns, url

from partners import views

urlpatterns = patterns('',
    # ex: /contacts/
    url(r'^$', views.index, name='partners_all'),
    # ex: /contacts/5/
    url(r'^(?P<partners_id>\d+)/$', views.detail, name='partners'),
	)
